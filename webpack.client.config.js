const path = require('path');
const config = require('./webpack.config');

module.exports = Object.assign({}, {
  entry: './src/containers/ClientApp.js',
  mode: 'development',
  output: {
    path: path.resolve(__dirname, './public/assets'),
    filename: 'client.bundle.js'
  }
}, config);