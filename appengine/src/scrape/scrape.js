
const XRay = require('x-ray');
const md5 = require('md5');
const get = require('lodash.get');
const slugify = require('slugify');
const moment = require('moment-timezone');
const values = require('lodash.values');
const chunk = require('lodash.chunk');
const toPairs = require('lodash.topairs');
const admin = require('firebase-admin');
const filters = require('./filters');

moment.locale('es');

const idify = text => slugify(text, {
  lower: true,
  remove: /[$*_+~.()'"¡!\-:@]/g
});

const x = new XRay({ filters });
const TIME_FORMAT = 'D MMM kk:mm';
const BASE_URL = 'https://www.fandango.lat';

// COUNTRIES

const getCountryCodes = () => (
  x(BASE_URL, ['.ctry-link@href | urlCountryCode | urlStripQuery'])
);

// THEATERS

const getTheatersUrls = url => (
  x(url, '.item__movietheater', ['.movietheater__name@href'])
);

const getTheater = url => (
  x(url, {
    data: 'script + script[type="application/ld+json"]'
  })
    .then(theater => {
      const data = JSON.parse(theater.data)

      const name = get(data, 'name');
      const address = get(data, 'location.address');
      const latitude = get(data, 'location.geo.latitude');
      const longitude = get(data, 'location.geo.longitude');
      const id = idify(name);

      return {
        id,
        name,
        address,
        latitude,
        longitude
      }

    })
);


// MOVIES

const getMoviesUrls = url => (
  x(url, '.movie', ['a@href'])
);

const getMovie = url => (
  x(url, {
    status: '.top__status | trim',
    details: '.top__details',
    backdrop: '.top__background@style | backgroundUrl',
    data: 'script + script[type="application/ld+json"]',
    scripts: ['script']
  })
    .then(movie => {

      const _data = JSON.parse(movie.data);
      const data = _data instanceof Array ? _data[0]: _data;

      const status = get(movie, 'status', '');
      const backdrop = get(movie, 'backdrop', '');

      const details = get(movie, 'details', '')
        .trim()
        .split(' | ');

      const scripts = get(movie, 'scripts', []);

      const videoScripts = scripts
        .filter(script => script.indexOf('videoId:') !== -1);

      const videoScript = get(videoScripts, '[0]');
      const videoId = videoScript ?
        videoScript
          .split('videoId: \'')[1]
          .split('\',')[0] : '';

      const year = get(details, '[2]');
      const premiere = status === 'estreno';

      const name = get(data, 'name');
      const description = get(data, 'description');
      const duration = get(data, 'duration');
      const poster = get(data, 'image.url');

      const id = idify(name);

      return {
        id,
        name,
        description,
        duration,
        poster,
        backdrop,
        year,
        premiere,
        videoId
      };
    })
);


// SHOWTIMES

const getMovieShowtimes = url => (
  x(url, {
    data: 'script + script[type="application/ld+json"]',
    date: '.item.date.active .date__number | trim',
    month: '.item.date.active .date__month | trim',
    showtimes: x('.accordion__body .accordion', [{
      theater: '.accordion__title h3',
      types: x('.time', [{
        labels: '.time__exhibit-mode',
        times: ['.time__date']
      }])
    }])
  })
    .paginate('.item.date.active + .item.date.inactive@href')
    .then(s => s.reduce((showtimes, movieShowtimes) => {

      const data = JSON.parse(movieShowtimes.data)
      const movieData = data instanceof Array ? data[0] : data;

      const movieName = get(movieData, 'name');
      const movieId = idify(movieName);
      const date = `${movieShowtimes.date} ${movieShowtimes.month}`;

      if (!movieShowtimes.showtimes) {
        return movieShowtimes;
      }

      movieShowtimes.showtimes.forEach(showtime => {

        const theaterId = idify(showtime.theater);

        if (!showtime.types) {
          return null;
        }

        return showtime.types.forEach(type => {
          return type.times.forEach(time => {
            const timeText = `${date} ${time}`;

            const showtimeData = {
              movieId,
              theaterId,
              labels: type.labels.toUpperCase(),
              time: moment(timeText, TIME_FORMAT).toDate()
            };

            const showtimeId = md5(JSON.stringify(showtimeData));
            showtimeData.id = showtimeId;
            showtimes[showtimeId] = showtimeData;
          });
        })
      })

      return showtimes;

    }, {}))
);


module.exports = {
  getCountryCodes,
  getTheatersUrls,
  getTheater,
  getMoviesUrls,
  getMovie,
  getMovieShowtimes
};