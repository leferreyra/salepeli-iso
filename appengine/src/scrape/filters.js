
const trim = value => value.trim();
const number = value => parseInt(value, 10);
const backgroundUrl = value => value.split('url(')[1].split(')')[0];
const urlCountryCode = value => value.split('/')[3];
const urlStripQuery = value => value.split('?')[0];

module.exports = {
  trim,
  number,
  backgroundUrl,
  urlCountryCode,
  urlStripQuery
}