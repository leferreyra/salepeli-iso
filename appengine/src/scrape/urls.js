
const getMoviesUrl = country => `https://www.fandango.lat/${country}/cartelera`;
const getTheatersUrl = country => `https://www.fandango.lat/${country}/cines`;

module.exports = {
  getMoviesUrl,
  getTheatersUrl
}