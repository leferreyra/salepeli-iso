
const PubSub = require('@google-cloud/pubsub');

const pubsub = new PubSub();
const empty = Buffer.from('');

const scrapeMovies = url => {
  return pubsub
    .topic('scrape-movies')
    .publisher()
    .publish(empty, { url })
}

const scrapeMovie = url => {
  return pubsub
    .topic('scrape-movie')
    .publisher()
    .publish(empty, { url })
}

const scrapeMovieShowtimes = url => {
  return pubsub
    .topic('scrape-showtimes')
    .publisher()
    .publish(empty, { url })
}

const scrapeTheaters = url => {
  return pubsub
    .topic('scrape-theaters')
    .publisher()
    .publish(empty, { url })
}

const scrapeTheater = url => {
  return pubsub
    .topic('scrape-theater')
    .publisher()
    .publish(empty, { url })
}

const updateScrapedData = (type, data) => {
  return pubsub
    .topic('update-scraped-data')
    .publisher()
    .publish(Buffer.from(JSON.stringify(data)), { type })
}

module.exports = {
  scrapeMovies,
  scrapeMovie,
  scrapeTheaters,
  scrapeTheater,
  updateScrapedData
}
