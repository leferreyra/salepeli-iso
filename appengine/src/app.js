
const express = require('express');
const bodyParser = require('body-parser');
const scrape = require('./scrape/scrape');
const urls = require('./scrape/urls');
const messages = require('./messages');

const app = express();
app.use(bodyParser.json());

const PUSH_PREFIX = '/_ah/push-handlers';

// Cron

// Push Handlers 

app.post(`${PUSH_PREFIX}/scrape-movies`, (req, res) => {
  const { attributes } = req.body.message;

  return scrape
    .getMoviesUrls(attributes.url)
    .then(urls => {
      return Promise.all(
        urls.map(messages.scrapeMovie)
      ).then(() => res.status(200).send())
    });
});

app.post(`${PUSH_PREFIX}/scrape-movie`, (req, res) => {
  const { attributes } = req.body.message;

  return res.status(200).send();

  return scrape
    .getMovie(attributes.url)
    .then(movie => {

      if (movie) {
        console.log('movie!', movie);
        return messages
          .updateScrapedData('movie', movie)
          .then(() => res.status(200).send())
          .catch(err => console.error(err))
      }

      console.log('No Movie! :(');
      return res.status(404).send();
    })
    .catch(err => console.error(err));
});

app.post(`${PUSH_PREFIX}/scrape-theaters`, (req, res) => {
  const { attributes } = req.body.message;

  return scrape
    .getTheatersUrls(attributes.url)
    .then(urls => {
      return Promise.all(
        urls.map(messages.scrapeTheater)
      ).then(() => res.status(200).send())
    });
});

app.post(`${PUSH_PREFIX}/scrape-theater`, (req, res) => {
  const { attributes } = req.body.message;

  console.log('url', attributes.url);

  return scrape
    .getTheater(attributes.url)
    .then(movie => {

      if (movie) {
        return messages
          .updateScrapedData('theater', movie)
          .then(() => res.status(200).send())
          .catch(err => console.error(err))
      }

      return res.status(404).send();
    });
});

module.exports = app;