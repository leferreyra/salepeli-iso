// @flow
import {
  INITIAL,
  LOADED,
  LOADING,
  ERROR,
  type LoadingState
} from 'constants/LoadingStateConstants';

export const isLoading = (state: LoadingState): boolean => {
  return state === LOADING;
}

export const isLoaded = (state: LoadingState): boolean => {
  return state === LOADED;
}

export const isError = (state: LoadingState): boolean => {
  return state === ERROR;
}