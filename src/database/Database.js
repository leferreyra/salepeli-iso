
function fromQuery(snapshot) {
  let docs = [];
  snapshot.forEach(doc => docs.push(doc));
  return docs.reduce((docMap, doc) => {
    docMap[doc.id] = doc.data()
    return docMap;
  }, {});
}

export default class Database {
  constructor(database) {
    this.db = database;
  }

  getCollectionMap(collection) {
    return this.db
      .collection(collection)
      .get()
      .then(fromQuery);
  }

  getCollectionDocument(collection, doc) {
    return this.db
      .collection(collection)
      .doc(doc)
      .get()
      .then(doc => doc.exists ? doc.data() : null);
  }

  getTheaters() {
    return this.getCollectionMap('theaters');
  }

  getMovies() {
    return this.getCollectionMap('movies');
  }

  getTheater(theaterId) {
    return this.getCollectionDocument('theaters', theaterId);
  }

  getMovie(movieId) {
    return this.getCollectionDocument('movies', movieId);
  }

  getTheatersMovies(theaterIds) {
    return Promise.all(
      theaterIds
        .map(theaterId =>
          this.db
            .collection('showtimes')
            .where('theaterId', '==', theaterId)
            .where('time', '>=', new Date())
            .get())
    ).then(snapshots => {
      return snapshots
        .reduce((movies, showtimeSnapshot) => {
          showtimeSnapshot.forEach(doc => {
            const showtime = doc.data();
            if (doc.exists && movies.indexOf(showtime.movieId) === -1) {
              movies.push(showtime.movieId);
            }
          });
          return movies;
        }, []);
    });
  }

  getMovieShowtimes(movieId, theaterId, fromDate, toDate) {
    return this.db
      .collection('showtimes')
      .where('movieId', '==', movieId)
      .where('theaterId', '==', theaterId)
      .where('time', '>=', fromDate)
      .where('time', '<', toDate)
      .get()
      .then(fromQuery);
  }
}