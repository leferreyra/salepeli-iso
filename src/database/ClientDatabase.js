// @flow

import Database from 'database/Database';
import { database } from 'firebase/Firebase';

export default new Database(database);
