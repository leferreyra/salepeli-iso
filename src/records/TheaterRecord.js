// @flow
import { Map, Record } from 'immutable';

export class TheaterRecord extends Record({
  id: '',
  name: '',
  address: '',
  latitude: 0,
  longitude: 0
}) {
  id: string;
  name: string;
  address: string;
  latitude: number;
  longitude: number;
}
