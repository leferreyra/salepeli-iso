// @flow
import { Map, Record } from 'immutable';

export class ShowtimeRecord extends Record({
  id: '',
  labels: '',
  movieId: '',
  theaterId: '',
  time: ''
}) {
  id: string;
  labels: string;
  movieId: string;
  theaterId: string;
  time: string;
}

export type ShowtimeData = {
  id: string;
  labels: string;
  movieId: string;
  theaterId: string;
  time: string;
};