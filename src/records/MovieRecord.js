// @flow
import { Map, Record } from 'immutable';

export class MovieRecord extends Record({
  id: '',
  name: '',
  description: '',
  duration: '',
  backdrop: '',
  poster: '',
  videoId: '',
  premiere: false
}) {
  id: string;
  name: string;
  description: string;
  duration: string;
  backdrop: string;
  poster: string;
  videoId: string;
  premiere: boolean;
}