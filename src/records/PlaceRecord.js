// @flow
import { List, Map, Record } from 'immutable';
import type { PlaceData } from 'apis/PlacesApi';

export class NearbyTheater extends Record({
  theaterId: '',
  distance: 0
}) {
  theaterId: string;
  distance: number;
};

export class PlaceRecord extends Record({
  id: '',
  name: '',
  location: Map(),
  timezone: '',
  nearbyTheaters: List(),
  nearbyMovies: List()
}) {
  id: string;
  name: string;
  location: Map<string, number>;
  timezone: string;
  nearbyTheaters: List<NearbyTheater>;
  nearbyMovies: List<string>;
}

export function createPlaceRecord(data: PlaceData): PlaceRecord {
  return new PlaceRecord({
    ...data,
    nearbyTheaters: List(data.nearbyTheaters).map(t => new NearbyTheater(t)),
    nearbyMovies: List(data.nearbyMovies)
  })
}
