// @flow

import React, { Component, type Node } from "react";
import GoogleAnalytics from "react-ga";
import { withRouter } from 'react-router-dom';

GoogleAnalytics.initialize('UA-111090567-1');

type Props = {
  location: {
    pathname: string;
  };
  children: Node;
};

const trackPage = page => GoogleAnalytics.pageview(page);

class GA extends Component<Props> {

  componentDidMount() {
    const page = this.props.location.pathname;
    trackPage(page);
  }

  componentDidUpdate(prevProps: Props) {

    const currentPage = prevProps.location.pathname;
    const nextPage = this.props.location.pathname;

    if (currentPage !== nextPage) {
      // Delay a bit to give a change for the title to update
      setTimeout(() => trackPage(nextPage), 100);
    }
  }

  render() {
    return this.props.children;
  }
};

export default withRouter(GA);