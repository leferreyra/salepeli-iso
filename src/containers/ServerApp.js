// @flow
import React, { Component } from 'react';
import App from '../components/App';
import { StaticRouter } from 'react-router';
import { StyleRoot } from 'radium';

type Props = {
  url: string;
  context: Object;
  radiumConfig: Object;
};

export default class ServerApp extends Component<Props> {
  render() {
    const { url, context, radiumConfig } = this.props;

    return (
      <StyleRoot radiumConfig={radiumConfig}>
        <StaticRouter location={url} context={context}>
          <App />
        </StaticRouter>
      </StyleRoot>
    );
  }
}
