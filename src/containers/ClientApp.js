// @flow
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import GoogleAnalytics from 'react-ga';
import WebFontLoader from 'webfontloader';
import App from '../components/App';
import GA from './GA';
import createStore from '../reducers/createStore';
import { Provider } from 'react-redux';
import { BrowserRouter } from 'react-router-dom';
import { StyleRoot } from 'radium';

const initialState = window.__initialState;
delete window.__initialState;

const store = createStore(initialState);

WebFontLoader.load({
  google: {
    families: ['Montserrat:200,300,400']
  }
});

const rootElement = document.getElementById('root');

if (rootElement) {
  ReactDOM.hydrate((
    <StyleRoot>
      <BrowserRouter>
        <GA>
          <Provider store={store}>
            <App />
          </Provider>
        </GA>
      </BrowserRouter>
    </StyleRoot>
  ), rootElement);
}