// @flow
import ClientDatabase from 'database/ClientDatabase';

export type MovieData = {
  id: string;
  name: string;
  backdrop?: string;
  poster?: string;
};

export const getMovie = (movieId: string): Promise<MovieData> => {
  return ClientDatabase.getMovie(movieId);
}