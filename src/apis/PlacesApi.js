// @flow

import { get } from 'axios';

const API_URL = '/api';

type NearbyTheaterData = {
  theaterId: string;
  distance: number;
}

export type PlaceData = {
  id: string;
  name: string;
  timezone: string;
  nearbyTheaters: Array<NearbyTheaterData>;
  nearbyMovies: Array<string>;
};

export const getPlace = (placeId: string): Promise<PlaceData> => {
  return get(`${API_URL}/place/${placeId}`)
    .then(response => response.data);
}

export type PlaceIdData = {
  placeId: string;
};

export const getPlaceIdFromCoords = (latitude: number, longitude: number): Promise<PlaceIdData> => {
  return get(`${API_URL}/coords/${latitude},${longitude}`)
    .then(response => response.data);
}