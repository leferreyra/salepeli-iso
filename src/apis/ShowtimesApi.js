// @flow
import moment from 'moment-timezone';
import values from 'lodash.values';
import ClientDatabase from 'database/ClientDatabase';

type ShowtimeData = {
  id: string;
  labels: string;
  movieId: string;
  theaterId: string;
  time: number;
};

export type ShowtimesData = {
  [string]: ShowtimeData;
};

type GetShowtimesParams = {
  movieId: string;
  theaterId: string;
  fromDate: number;
  toDate: number;
};
export const getShowtimes = (params: GetShowtimesParams): Promise<ShowtimesData> => {
  const { movieId, theaterId, fromDate, toDate } = params;

  return ClientDatabase.getMovieShowtimes(
    movieId,
    theaterId,
    moment.unix(fromDate).toDate(),
    moment.unix(toDate).toDate()
  )
    .then(showtimes => {
      return values(showtimes)
        .reduce((map, showtime) => {
          map[showtime.id] = {
            ...showtime,
            time: moment
              .unix(showtime.time.seconds)
              .toISOString()
          }
          return map;
        }, {})
    });
};