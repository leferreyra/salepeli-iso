// @flow
import ClientDatabase from 'database/ClientDatabase';

export type TheaterData = {
  id: string;
  name: string;
  address: string;
  latitude: number;
  longitude: number;
};

export const getTheater = (theaterId: string): Promise<TheaterData> => {
  return ClientDatabase.getTheater(theaterId);
}