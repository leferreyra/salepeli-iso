// @flow

import get from 'lodash.get';
import moment from 'moment-timezone';
import { createSelector } from 'reselect';
import { ShowtimesKeyRecord } from 'reducers/ShowtimesReducer';

import type { List } from 'immutable';
import type { State } from 'reducers/createStore';
import type { PlaceRecord } from 'records/PlaceRecord';
import type { MovieRecord } from 'records/MovieRecord';
import type { TheaterRecord } from 'records/TheaterRecord';
import type { ShowtimeRecord } from 'Records/ShowtimeRecord';

// common selectors

export const appSelector = (state: State) => state.app;
export const placesSelector = (state: State) => state.places;
export const moviesSelector = (state: State) => state.movies;
export const theatersSelector = (state: State) => state.theaters;
export const showtimesSelector = (state: State) => state.showtimes;


// route selectors

type RouterPlaceIdProps = {
  match: {
    params: {
      placeId: string;
    }
  }
}
export const placeIdFromRouterSelector = (state: State, props: RouterPlaceIdProps): ?string => {
  return get(props, 'match.params.placeId', null);
}

type RouterMovieIdProps = {
  match: {
    params: {
      movieId: string;
    }
  }
}
export const movieIdFromRouterSelector = (state: State, props: RouterMovieIdProps): ?string => {
  return get(props, 'match.params.movieId', null);
}


// props selectors

export const movieIdFromPropsSelector = (state: State, props: { movieId: string }): string => {
  return get(props, 'movieId', null);
}

export const movieFromPropsSelector = createSelector(
  movieIdFromPropsSelector,
  moviesSelector,
  (movieId, movies): ?MovieRecord => {
    return movies.getIn([movieId, 'movie'], null);
  }
)

export const theaterIdFromPropsSelector = (state: State, props: { theaterId: string }): string => {
  return get(props, 'theaterId', null);
}

export const theaterFromPropsSelector = createSelector(
  theaterIdFromPropsSelector,
  theatersSelector,
  (theaterId, theaters): ?TheaterRecord => {
    return theaters.getIn([theaterId, 'theater'], null);
  }
)

export const dateFromPropsSelector = (state: State, props: { date: number }): number => {
  return get(props, 'date', null);
}

// advanced selectors

export const selectedPlaceSelector = createSelector(
  placeIdFromRouterSelector,
  placesSelector,
  (placeId, places): ?PlaceRecord => {
    return places.getIn([placeId, 'place'], null);
  }
)

export const timezoneSelector = createSelector(
  selectedPlaceSelector,
  (place): string => {
    return place ? place.get('timezone', '') : '';
  }
)

export const selectedMovieSelector = createSelector(
  movieIdFromRouterSelector,
  moviesSelector,
  (movieId, movies): ?MovieRecord => {
    return movies.getIn([movieId, 'movie'], null);
  }
)

export const selectedShowtimesSelector = createSelector(
  movieIdFromRouterSelector,
  theaterIdFromPropsSelector,
  dateFromPropsSelector,
  showtimesSelector,
  timezoneSelector,
  (movieId, theaterId, date, showtimes, timezone): ?List<ShowtimeRecord> => {

    const momentDate = moment.unix(date);
    const d = timezone ? momentDate.tz(timezone) : momentDate;
    const fromDate = d.startOf('day').unix();
    const toDate = d.endOf('day').unix();

    const key = new ShowtimesKeyRecord({
      movieId,
      theaterId,
      fromDate,
      toDate
    });

    return showtimes.getIn([key, 'showtimes'], null);
  }
)

export const latestMoviesIdsSelector = createSelector(
  appSelector,
  (app): ?string => app.latestMovies
)

export const geolocationSelector = createSelector(
  appSelector,
  (app): ?string => app.geolocation
)

export const savedPlaceIdSelector = createSelector(
  geolocationSelector,
  (geolocation): ?string => geolocation.placeId
)
