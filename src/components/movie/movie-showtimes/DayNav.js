// @flow
import React, { Component } from 'react';
import Radium from 'radium';
import moment from 'moment-timezone';
import ChevronRight from 'react-icons/lib/md/chevron-right';

moment.locale('es');

const formatDay = date => date.calendar(null, {
  sameDay: '[Hoy]',
  nextDay: '[Mañana]',
  nextWeek: 'dddd'
});

type Props = {
  startDate: number;
  selectedDate: number;
  onChange: (date: number) => void;
};

type State = {
  open: boolean;
};

class DayNav extends Component<Props, State> {

  state = {
    open: false
  }

  toggle = () => {
    this.setState({ open: !this.state.open });
  }

  change = date => () => {
    this.props.onChange(date);
    this.setState({ open: false });
  }

  renderDays() {
    const { startDate, selectedDate } = this.props;

    const selected = moment.unix(selectedDate);
    const date = moment.unix(startDate);
    const days = [];

    while (date.day() !== 4) {

      if (!date.startOf('day').isSame(selected.startOf('day'))) {
        days.push(
          <li
            key={date.day()}
            style={styles.day}
            onClick={this.change(date.unix())}>

            {formatDay(date)}
          </li>
        );
      }

      date.add(1, 'days');
    }

    return days;
  }

  render() {
    const { selectedDate } = this.props;
    const { open } = this.state;

    return (
      <div style={styles.root}>
        <h3 style={styles.selected} onClick={this.toggle}>
          {formatDay(moment.unix(selectedDate))}
          {/*<ChevronRight style={styles.chevron} />*/}
        </h3>
        {open &&
          <div style={styles.days}>
            <ul style={styles.list}>
              {this.renderDays()}
            </ul>
          </div>}
      </div>
    );
  }
}

const styles = {
  root: {
    margin: '30px 0',
    display: 'flex',
    fontWeight: 'lighter',
    alignItems: 'center'
  },
  selected: {
    margin: 0,
    flex: '0 0 auto',
    display: 'block',
    textTransform: 'uppercase',
    cursor: 'pointer',
    fontWeight: 'lighter',
  },
  chevron: {
    position: 'relative',
    top: -2,
    fill: '#FF0000'
  },
  days: {
    height: 18,
    overflow: 'hidden',
    flex: '1 1 auto',
    display: 'inline-block'
  },
  list: {
    width: '100%',
    overflowX: 'scroll',
    listStyle: 'none',
    padding: '0 0 50px 0',
    margin: '0 0 -50px 0'
  },
  day: {
    cursor: 'pointer',
    color: '#A7A7A7',
    display: 'inline',
    textTransform: 'uppercase',
    marginLeft: 15,
    transition: 'color 0.3s ease-in-out',

    ':hover': {
      color: '#FF0000'
    }
  }
};

export default Radium(DayNav);