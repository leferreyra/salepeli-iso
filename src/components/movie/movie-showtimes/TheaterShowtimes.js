// @flow
import React, { Component } from 'react';
import Radium from 'radium';
import moment from 'moment-timezone';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import {
  theaterIdFromPropsSelector,
  theaterFromPropsSelector,
  movieIdFromRouterSelector,
  selectedShowtimesSelector,
  timezoneSelector,
} from 'selectors/SharedSelectors';
import { loadShowtimes, type LoadShowtimesAction } from 'actions/ShowtimesActions';
import { loadTheater, type LoadTheaterAction } from 'actions/TheatersActions';

import type { List } from 'immutable';
import type { ShowtimeRecord } from 'records/ShowtimeRecord';
import type { TheaterRecord } from 'records/TheaterRecord';

type Actions = {
  loadShowtimes: LoadShowtimesAction;
  loadTheater: LoadTheaterAction;
};

type Props = Actions & {
  theaterId: string;
  theater: ?TheaterRecord;
  movieId: string;
  date: number;
  showtimes: List<ShowtimeRecord>;
  timezone: string;
};

const groupByLabels = (showtime: ShowtimeRecord) => showtime.labels;

class TheaterShowtimes extends Component<Props> {

  componentDidMount() {
    const {
      date,
      movieId,
      theaterId,
      showtimes,
      theater,
      loadShowtimes,
      loadTheater
    } = this.props;

    const momentDate = moment.unix(date);
    const fromDate = momentDate.startOf('day').unix();
    const toDate = momentDate.endOf('day').unix();

    if (!showtimes) {
      loadShowtimes({
        movieId,
        theaterId,
        fromDate,
        toDate
      });
    }

    if (!theater) {
      loadTheater(theaterId);
    }
  }

  renderLabelGroup(labels: string, showtimes: List<ShowtimeRecord>) {
    const { timezone } = this.props;

    return (
      <div key={labels} style={styles.group}>
        <h3 style={styles.labels}>{labels}</h3>
        <ul style={styles.list}>
          {showtimes.map(showtime => (
            <li style={styles.time} key={showtime.id}>
              {moment(showtime.time)
                .tz(timezone)
                .format('H:mm')}
            </li>
          ))}
        </ul>
      </div>
    );
  }

  render() {
    const { theater, showtimes } = this.props;

    if (!theater || !showtimes || showtimes.isEmpty()) {
      return null;
    }

    return (
      <div style={styles.root}>
        <h2 style={styles.name}>{theater.name}</h2>

        {showtimes
          .groupBy(groupByLabels)
          .entrySeq()
          .map(([labels, group]) => (
            this.renderLabelGroup(labels, group)
          ))}

      </div>
    );
  }
}

const styles = {
  root: {
    marginBottom: 30
  },
  name: {
    fontSize: 20,
    fontWeight: 300,
    color: '#FF0700',
    margin: 0
  },
  group: {},
  labels: {
    fontSize: 16,
    color: '#696969',
    fontWeight: 300,
    margin: '25px 0 0 0'
  },
  list: {
    listStyle: 'none',
    margin: '10px 0 0 -10px',
    padding: 0
  },
  time: {
    display: 'inline-block',
    margin: '10px 0 0 10px',
    width: 120,
    height: 64,
    lineHeight: '64px',
    textAlign: 'center',
    fontSize: 30,
    fontWeight: 'lighter',
    border: '1px solid #A5A5A5',
    borderRadius: 6
  }
};

const selector = createSelector(
  theaterIdFromPropsSelector,
  theaterFromPropsSelector,
  movieIdFromRouterSelector,
  selectedShowtimesSelector,
  timezoneSelector,
  (
    theaterId: string,
    theater: ?TheaterRecord,
    movieId: string,
    showtimes: ?List<ShowtimeRecord>,
    timezone: string
  ) => {
    return {
      theaterId,
      theater,
      movieId,
      showtimes,
      timezone
    };
  }
);

const actions = {
  loadShowtimes,
  loadTheater,
};

export default withRouter(connect(selector, actions)(Radium(TheaterShowtimes)));