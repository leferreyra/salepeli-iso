// @flow
import React, { Component } from 'react';
import Radium from 'radium';
import DayNav from 'components/movie/movie-showtimes/DayNav';
import TheaterShowtimes from 'components/movie/movie-showtimes/TheaterShowtimes';
import moment from 'moment-timezone';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { Helmet } from 'react-helmet';
import {
  placeIdFromRouterSelector,
  selectedPlaceSelector,
  movieIdFromRouterSelector,
  selectedMovieSelector
} from 'selectors/SharedSelectors';
import { loadPlace, type LoadPlaceAction } from 'actions/PlacesActions';
import { loadMovie, type LoadMovieAction } from 'actions/MoviesActions';

import type { List } from 'immutable';
import type { PlaceRecord } from 'records/PlaceRecord';
import type { MovieRecord } from 'records/MovieRecord';

type Actions = {
  loadPlace: LoadPlaceAction;
  loadMovie: LoadMovieAction;
};

type Props = Actions & {
  placeId: string;
  place: ?PlaceRecord;
  movieId: string;
  movie: ?MovieRecord;
};

type State = {
  date: number;
};

class MovieShowtimes extends Component<Props, State> {

  state = {
    date: moment().unix()
  }

  componentDidMount() {
    const {
      placeId,
      place,
      loadPlace,
      movieId,
      movie,
      loadMovie,
    } = this.props;

    if (!place) {
      loadPlace(placeId);
    }

    if (!movie) {
      loadMovie(movieId);
    }
  }

  onDateChange = date => {
    this.setState({ date });
  }

  render() {
    const { movieId, movie, placeId, place } = this.props;
    const { date } = this.state;

    if (!movie || !place) {
      return null;
    }

    return (
      <div style={styles.root}>

        <Helmet>
          <title>Horarios para {movie.name} en {place.name}</title>
          <meta property="og:title" content={`Horarios para ${movie.name} en ${place.name}`} />
        </Helmet>

        <DayNav
          startDate={moment().unix()}
          selectedDate={date}
          onChange={this.onDateChange} />

        {place.nearbyTheaters.map(nearbyTheater => (
          <TheaterShowtimes
            date={date}
            key={nearbyTheater.theaterId}
            theaterId={nearbyTheater.theaterId} />
        ))}

      </div>
    );
  }
}

const styles = {
  root: {
    marginBottom: 30
  }
};

const selector = createSelector(
  placeIdFromRouterSelector,
  selectedPlaceSelector,
  movieIdFromRouterSelector,
  selectedMovieSelector,
  (
    placeId: string,
    place: ?PlaceRecord,
    movieId: string,
    movie: ?MovieRecord
  ) => {
    return {
      placeId,
      place,
      movieId,
      movie
    };
  }
);

const actions = {
  loadPlace,
  loadMovie,
};

export default connect(selector, actions)(Radium(MovieShowtimes));