// @flow
import React, { Component, Fragment } from 'react';
import Radium from 'radium';
import { Redirect } from 'react-router-dom';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { Helmet } from 'react-helmet';
import {
  savedPlaceIdSelector,
  movieIdFromRouterSelector,
  selectedMovieSelector
} from 'selectors/SharedSelectors';

import type { PlaceRecord } from 'records/PlaceRecord';
import type { MovieRecord } from 'records/MovieRecord';

type Props = {
  placeId?: string;
  movieId: string;
  movie: ?MovieRecord;
  match: {
    url: string;
  }
};

class MovieShowtimesNoPlace extends Component<Props> {
  render() {
    const { movieId, movie, placeId, match: { url } } = this.props;

    return (
      <Fragment>

        <Helmet>
          {movie && <title>Horarios para {movie.name}</title>}
        </Helmet>

        {placeId ? <Redirect to={`${url}/${placeId}`} /> : <h1>You need to enable geolocation</h1>}

      </Fragment>
    );
  }
}

const styles = {};

const selector = createSelector(
  savedPlaceIdSelector,
  movieIdFromRouterSelector,
  selectedMovieSelector,
  (
    placeId: ?string,
    movieId: string,
    movie: ?MovieRecord
  ) => {
    return {
      placeId,
      movieId,
      movie
    };
  }
);

export default connect(selector, null)(Radium(MovieShowtimesNoPlace));