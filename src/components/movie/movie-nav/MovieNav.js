// @flow
import React, { Component, type Node } from 'react';
import Radium from 'radium';
import get from 'lodash.get';
import { withRouter, NavLink } from 'react-router-dom';

type Props = {
  movieId: string;
};

class MovieNav extends Component<Props> {
  render() {
    const { movieId } = this.props;

    return (
      <ul style={styles.root}>

        <li style={styles.item}>
          <NavLink
            style={styles.link}
            activeStyle={styles.activeLink}
            to={`/m/${movieId}/s`}>
            Horarios
          </NavLink>
        </li>

        <li style={styles.item}>
          <NavLink
            exact
            style={styles.link}
            activeStyle={styles.activeLink}
            to={`/m/${movieId}`}>
            Información
          </NavLink>
        </li>

      </ul>
    )
  }
}

const styles = {
  root: {
    display: 'flex',
    alignItems: 'stretch',
    listStyle: 'none',
    padding: '0 20px',
    margin: 0,
    backgroundColor: '#E6E6E6',
    height: 52,
    borderRadius: '0 0 5px 5px',

    '@media (max-width: 920px)': {
      borderRadius: 0
    }
  },
  item: {
    display: 'flex',
    alignItems: 'stretch',
    marginRight: 20
  },
  link: {
    display: 'flex',
    alignItems: 'center',
    color: '#505050',
    fontSize: 14,
    textTransform: 'uppercase',
    textDecoration: 'none',
    borderTop: '2px solid transparent',
    borderBottom: '2px solid transparent'
  },
  activeLink: {
    color: '#FF0000',
    borderBottom: '2px solid #FF0000'
  }
};

export default withRouter(Radium(MovieNav));