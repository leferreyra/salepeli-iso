// @flow
import React, { Component, Fragment } from 'react';
import Radium from 'radium';
import { Helmet } from 'react-helmet';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import {
  movieIdFromRouterSelector,
  selectedMovieSelector,
  savedPlaceIdSelector
} from 'selectors/SharedSelectors';

import type { MovieRecord } from 'records/MovieRecord';

type Props = {
  movieId: string;
  movie: ?MovieRecord;
  placeId: ?string;
};

class MovieInfo extends Component<Props> {

  render() {
    const { movieId, movie, placeId } = this.props;

    if (!movie) {
      return null;
    }

    return (
      <Fragment>
        {movie &&
          <Helmet>
            <title>{movie.name}</title>
            <meta name='description' content={movie.description} />

            <meta property="og:title" content={movie.name} />
            <meta property="og:description" content={movie.description} />
          </Helmet>}
        <p>
          {movie.description}
        </p>
      </Fragment>
    );
  }
}

const styles = {};

const selector = createSelector(
  movieIdFromRouterSelector,
  selectedMovieSelector,
  (movieId: string, movie: ?MovieRecord) => {
    return { movieId, movie };
  }
);

export default connect(selector, null)(Radium(MovieInfo));