// @flow
import React, { Component } from 'react';
import Radium from 'radium';
import YouTube from 'react-youtube';
import PlayArrow from 'react-icons/lib/md/play-arrow';

type Props = {
  title: string;
  backdrop: string;
  videoId: ?string;
};

type State = {
  showVideo: boolean;
};

class BackdropTitle extends Component<Props, State> {

  state = {
    showVideo: false
  }

  onVideoEnd = () => {
    this.setState({ showVideo: false });
  }

  toggleVideo = () => {
    this.setState({ showVideo: !this.state.showVideo });
  }

  render() {
    const { title, backdrop, videoId } = this.props;
    const { showVideo } = this.state;

    return (
      <div style={styles.root}>

        <img src={backdrop} alt={title} style={[
          styles.backdrop,
          styles.fade,
          showVideo && styles.hide
        ]} />

        {videoId && showVideo ?
          <YouTube
            videoId={videoId}
            onEnd={this.onVideoEnd}
            className='youtube-player'
            opts={{
              playerVars: {
                autoplay: 1
              }
            }} /> :
          <div style={styles.overlay}>

            {videoId &&
              <div style={styles.play}>
                <PlayArrow style={styles.playIcon} onClick={this.toggleVideo} />
              </div>}

            <h1 style={styles.title}>{title}</h1>

          </div>}

      </div>
    )
  }
}

const styles = {
  root: {
    height: 0,
    position: 'relative',
    backgroundColor: 'black',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    paddingBottom: '56%',
    overflow: 'hidden'
  },
  fade: {
    transition: 'opacity 1s ease-in-out'
  },
  hide: {
    opacity: 0
  },
  backdrop: {
    position: 'absolute',
    top: 0,
    width: '100%',
  },
  overlay: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    backgroundColor: 'rgba(0, 0, 0, 0.3)'
  },
  title: {
    color: 'white',
    fontWeight: 300,
    fontSize: 26,
    margin: 0,
    padding: 20,
    position: 'absolute',
    bottom: 0,
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textOverflow: 'ellipsis',
    boxSizing: 'border-box',
    width: '100%',
    lineHeight: '1em',

    '@media (max-width: 550px)': {
      fontSize: 22
    }
  },
  play: {
    position: 'absolute',
    top: '50%',
    left: '50%',
    transform: 'translate(-50%, -50%)',
    opacity: 0.5,
    cursor: 'pointer',
    transition: 'opacity 0.3s easy-in-out',

    ':hover': {
      opacity: 1
    }
  },
  playIcon: {
    fill: 'white',
    width: 50,
    height: 50
  }
};

export default Radium(BackdropTitle);