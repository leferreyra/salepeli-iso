// @flow

import React, { Component, type Node } from 'react';
import Radium from 'radium';
import { Route, Link } from 'react-router-dom';

import logo from 'images/logo.svg';

type Props = {
  children: Node;
  style?: Object;
};

class TopBar extends Component<Props> {
  render() {
    const { children, style } = this.props;
    return (
      <div style={[styles.root, style]}>
        {this.props.children}
      </div>
    )
  }
}

const styles = {
  root: {
    maxWidth: 900,
    marginLeft: 'auto',
    marginRight: 'auto',
    paddingLeft: 20,
    paddingRight: 20
  }
};

export default Radium(TopBar);