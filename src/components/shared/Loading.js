// @flow

import React, { Component, type Node } from 'react';
import Radium from 'radium';

import LOGO from 'images/iso.svg';

type Props = {
  style?: Object;
};

class Loading extends Component<Props> {
  render() {
    const { style } = this.props;
    return (
      <div style={styles.root}>
        <img src={LOGO} style={styles.spinner} />
      </div>
    )
  }
}

const styles = {
  root: {
    position: 'absolute',
    height: '100%',
    width: '100%',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  spinner: {
    width: 60,
    height: 60
  }
};

export default Radium(Loading);