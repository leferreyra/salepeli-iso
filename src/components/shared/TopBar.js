// @flow

import React, { Component } from 'react';
import Container from 'components/shared/Container';
import { Route, Link } from 'react-router-dom';
import logo from 'images/logo.svg';

import type { PlaceRecord } from 'records/PlaceRecord';

export const TOP_BAR_HEIGHT = 62;

type Props = {
  homeUrl: string;
};

export default class TopBar extends Component<Props> {
  render() {
    const { homeUrl} = this.props;

    return (
      <nav style={styles.root}>
        <Container style={styles.container}>
          <Link to={homeUrl} style={styles.homeLink}>
            <img style={styles.logo} src={logo} alt='Sale Peli' />
          </Link>
        </Container>
      </nav>
    )
  }
}

const styles = {
  root: {
    zIndex: 1000,
    background: '#F8F8F8',
    width: '100%',
    position: 'fixed',
    top: 0
  },
  container: {
    height: TOP_BAR_HEIGHT,
    display: 'flex',
    alignItems: 'stretch'
  },
  homeLink: {
    display: 'flex',
    alignItems: 'center'
  },
  logo: {
    width: 130
  }
};