// @flow
import React, { Component, type Node } from 'react';
import Radium from 'radium';
import { Route, Link } from 'react-router-dom';

type Props = {
  children: Node;
  style?: Object;
};

class Button extends Component<Props> {
  render() {
    const { children, style, ...rest } = this.props;
    return (
      <button style={[styles.root, style]} {...rest} >
        {this.props.children}
      </button>
    )
  }
}

const styles = {
  root: {
    backgroundColor: '#00C4FF',
    color: 'white',
    border: 0,
    borderRadius: 5,
    padding: '12px 40px',
    fontSize: 17,
    fontFamily: 'Montserrat',
    fontWeight: 'lighter',
    cursor: 'pointer',
    transition: 'background 0.3s ease-in-out',
    outline: 0,

    ':hover': {
      backgroundColor: '#00B5EB'
    }
  }
};

export default Radium(Button);