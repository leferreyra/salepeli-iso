// @flow
import React, { Component, Fragment } from 'react';
import Radium from 'radium';
import TopBar, { TOP_BAR_HEIGHT } from 'components/shared/TopBar';
import Container from 'components/shared/Container';
import Button from 'components/shared/Button';
import MovieList from 'components/movie-list/MovieList';
import Spinner from 'react-md-spinner';
import { Redirect } from 'react-router-dom';
import { createSelector } from 'reselect';
import {
  latestMoviesIdsSelector,
  geolocationSelector
} from 'selectors/SharedSelectors';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import {
  loadPlace,
  type LoadPlaceAction
} from 'actions/PlacesActions';
import {
  getLocation,
  type GetLocation
} from 'actions/AppActions';
import { List } from 'immutable';
import { isLoading } from 'utils/StateUtils';

import type { PlaceRecord } from 'records/PlaceRecord';
import type { TheaterRecord } from 'records/TheaterRecord';
import type { MovieRecord } from 'records/MovieRecord';
import type { GeolocationStateRecord } from 'reducers/AppReducer';

type Actions = {
  loadPlace: LoadPlaceAction;
  getLocation: GetLocation;
}

type Props = Actions & {
  geolocation: GeolocationStateRecord;
  movieIds: List<string>;
};

class Start extends Component<Props> {
  render() {
    const { movieIds, geolocation, getLocation } = this.props;

    if (geolocation.placeId) {
      return <Redirect to={`/p/${geolocation.placeId}`} />;
    }

    return (
      <Fragment>

        <Helmet>
          <title>Sale Peli!</title>
          <meta
            name='description'
            content={`Horarios, información y trailers de películas en cines cercanos`} />
        </Helmet>

        <TopBar homeUrl='/' />

        <Container style={styles.container}>

          <div style={styles.hero}>
            <h1 style={styles.title}>
              Horarios de películas en todos los cines
              cercanos en un solo lugar.
            </h1>

            <div style={styles.callToAction}>
              {isLoading(geolocation.state) ?
                <Spinner singleColor='#00B5EB' />:
                <Fragment>
                  <p style={styles.helpText}>
                    Para comenzar debes compartirnos tu ubicación.
                  </p>
                  <Button onClick={getLocation}>
                    Activar ubicación
                  </Button>
                </Fragment>}
            </div>
          </div>

          <MovieList movieIds={movieIds} />

        </Container>

      </Fragment>
    )
  }
}

const styles = {
  container: {
    marginTop: TOP_BAR_HEIGHT,
    paddingTop: 20,
    paddingBottom: 30
  },
  hero: {
    margin: '50px 0',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    textAlign: 'center',
  },
  title: {
    maxWidth: 540,
    fontWeight: 'normal',
    color: '#7B7B7B',
    fontSize: 27,
    margin: 0,

    '@media (max-width: 510px)': {
      fontSize: 18
    }
  },
  callToAction: {
    height: 96,
    marginTop: 30,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  },
  helpText: {
    margin: '0 0 12px 0',
    fontSize: 14,
    fontWeight: 'lighter',
    color: '#8B8B8B',
    maxWidth: 200,
  },
}

const selector = createSelector(
  latestMoviesIdsSelector,
  geolocationSelector,
  (movieIds: List<string>, geolocation: GeolocationStateRecord) => {
    return {
      movieIds,
      geolocation
    }
  }
);

const actions = {
  loadPlace,
  getLocation
};

export default connect(selector, actions)(Radium(Start));