// @flow
import React, { Component, Fragment } from 'react';
import TopBar, { TOP_BAR_HEIGHT } from 'components/shared/TopBar';
import Container from 'components/shared/Container';
import MovieList from 'components/movie-list/MovieList';
import Loading from 'components/shared/Loading';
import { createSelector } from 'reselect';
import {
  placeIdFromRouterSelector,
  selectedPlaceSelector
} from 'selectors/SharedSelectors';
import { connect } from 'react-redux';
import { createAction } from 'redux-actions';
import { Link, withRouter } from 'react-router-dom';
import { Helmet } from 'react-helmet';
import { Map } from 'immutable';
import { loadPlace, type LoadPlaceAction } from 'actions/PlacesActions';

import type { PlaceRecord } from 'records/PlaceRecord';
import type { TheaterRecord } from 'records/TheaterRecord';
import type { MovieRecord } from 'records/MovieRecord';

type Actions = {
  loadPlace: LoadPlaceAction;
}

type Props = Actions & {
  placeId: string;
  place: ?PlaceRecord;
};

class Home extends Component<Props> {

  componentDidMount() {
    const { placeId, place, loadPlace } = this.props;

    if (!place) {
      loadPlace(placeId);
    }
  }

  render() {
    const { place } = this.props;

    if (!place) {
      return <Loading />;
    }

    return (
      <Fragment>

        <Helmet>
          <title>Cartelera de cine para {place.name}</title>
          <meta
            name='description'
            content={`Horarios, información y trailers de peliculas en cines de ${place.name}`} />
        </Helmet>

        <TopBar homeUrl={`/p/${place.id}`} />

        <Container style={styles.container}>

          <h1 style={styles.title}> Cartelera para <span style={styles.placeName}>{place.name}</span></h1>

          <div style={styles.movieList}>
            <MovieList movieIds={place.nearbyMovies} placeId={place.id} />
          </div>

        </Container>

      </Fragment>
    )
  }
}

const styles = {
  container: {
    marginTop: TOP_BAR_HEIGHT,
    paddingTop: 20,
    paddingBottom: 30
  },
  title: {
    margin: 0,
    padding: '10px 0',
    fontSize: 25,
    fontWeight: 'lighter',
    color: 'gray',

    '@media (max-width: 650px)': {
      fontSize: 22,
      padding: 0
    }
  },
  placeName: {
    color: '#FF1F00'
  },
  movieList: {}
}

const selector = createSelector(
  placeIdFromRouterSelector,
  selectedPlaceSelector,
  (placeId: string, place) => {
    return {
      placeId,
      place
    }
  }
);

const actions = {
  loadPlace
};

export default connect(selector, actions)(Home);