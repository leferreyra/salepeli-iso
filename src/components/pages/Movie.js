// @flow
import React, { Component, Fragment } from 'react';
import TopBar, { TOP_BAR_HEIGHT } from 'components/shared/TopBar';
import Container from 'components/shared/Container';
import Loading from 'components/shared/Loading';
import BackdropTitle from 'components/shared/BackdropTitle';
import MovieShowtimes from 'components/movie/movie-showtimes/MovieShowtimes';
import MovieShowtimesNoPlace from 'components/movie/movie-showtimes/MovieShowtimesNoPlace';
import MovieInfo from 'components/movie/movie-info/MovieInfo';
import MovieNav from 'components/movie/movie-nav/MovieNav';
import { Helmet } from 'react-helmet';
import { createSelector } from 'reselect';
import {
  movieIdFromRouterSelector,
  selectedMovieSelector,
  savedPlaceIdSelector
} from 'selectors/SharedSelectors';
import { connect } from 'react-redux';
import { Route, withRouter } from 'react-router-dom';
import { Map } from 'immutable';
import { loadPlace, type LoadPlaceAction } from 'actions/PlacesActions';

import type { PlaceRecord } from 'records/PlaceRecord';
import type { TheaterRecord } from 'records/TheaterRecord';
import type { MovieRecord } from 'records/MovieRecord';

type Props = {
  movieId: string;
  movie: ?MovieRecord;
  placeId: ?string;
  match: {
    path: string;
  }
};

class Movie extends Component<Props> {
  render() {
    const { movieId, movie, placeId, match: { path } } = this.props;

    if (!movie) {
      return <Loading />;
    }

    return (
      <Fragment>

        <Helmet>
          <meta property="og:image" content={movie.backdrop} />
        </Helmet>

        <TopBar homeUrl={placeId ? `/p/${placeId}` : '/'} />

        <Container style={styles.container}>

          <div style={styles.header}>
            <BackdropTitle
              title={movie.name}
              backdrop={movie.backdrop}
              videoId={movie.videoId} />

            <MovieNav movieId={movieId} />
          </div>

          <Route exact path={path} component={MovieInfo} />
          <Route exact path={`${path}/s`} component={MovieShowtimesNoPlace} />
          <Route exact path={`${path}/s/:placeId`} component={MovieShowtimes} />

        </Container>

      </Fragment>
    )
  }
}

const styles = {
  container: {
    marginTop: TOP_BAR_HEIGHT
  },
  header: {
    margin: '0 -20px'
  }
}

const selector = createSelector(
  movieIdFromRouterSelector,
  selectedMovieSelector,
  savedPlaceIdSelector,
  (
    movieId: string,
    movie: ?MovieRecord,
    placeId: ?string
  ) => {
    return {
      movieId,
      movie,
      placeId
    }
  }
);

export default connect(selector, null)(Movie);