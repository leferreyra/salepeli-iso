// @flow

import React, { Component, type Node } from 'react';
import Radium from 'radium';
import MovieListItem from 'components/movie-list/MovieListItem';
import { Route, Link } from 'react-router-dom';
import { Set } from 'immutable';

type Props = {
  movieIds: Set<string>;
  placeId?: string;
};

class MovieList extends Component<Props> {
  render() {
    const { movieIds, placeId } = this.props;

    return (
      <ul style={styles.root}>
        {movieIds.map(movieId => (
          <li key={movieId} style={styles.item}>
            {placeId ?
              <MovieListItem movieId={movieId} />:
              <MovieListItem movieId={movieId} placeId={placeId}/>}
          </li>
        ))}
      </ul>
    );
  }
}

const styles = {
  root: {
    display: 'flex',
    alignItems: 'space-between',
    flexWrap: 'wrap',
    listStyle: 'none',
    padding: 0,
    margin: '0 0 0 -20px'
  },
  item: {
    flex: '0 0 20%',
    boxSizing: 'border-box',
    paddingLeft: 20,
    paddingTop: 20,

    '@media (max-width: 800px)': {
      flex: '0 0 25%'
    },

    '@media (max-width: 670px)': {
      flex: '0 0 33.3%'
    },

    '@media (max-width: 500px)': {
      flex: '0 0 50%'
    },
  }
};

export default Radium(MovieList);