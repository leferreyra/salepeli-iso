// @flow

import React, { Component, type Node } from 'react';
import Radium from 'radium';
import { Link, withRouter } from 'react-router-dom';
import { createSelector } from 'reselect';
import { movieFromPropsSelector, placeIdFromRouterSelector } from 'selectors/SharedSelectors';
import { connect } from 'react-redux';
import { loadMovie, type LoadMovieAction } from 'actions/MoviesActions';

import type { MovieRecord } from 'records/MovieRecord';

type MappedState = {
  movie: ?MovieRecord;
};

type Actions = {
  loadMovie: LoadMovieAction;
};

type Props = MappedState & Actions & {
  placeId?: string;
  movieId: string;
};

class MovieListItem extends Component<Props> {

  componentDidMount() {
    const { movieId, movie, loadMovie } = this.props;

    if (!movie) {
      loadMovie(movieId);
    }
  }

  render() {
    const { placeId, movieId, movie } = this.props;

    return (
      <Link to={placeId ? `/m/${movieId}/s/${placeId}` : `/m/${movieId}`}>
        <div style={[styles.root, movie && {
          backgroundImage: `url(${movie.poster})`
        }]} />
      </Link>
    );
  }
}

const styles = {
  root: {
    width: '100%',
    paddingBottom: '148%',
    backgroundColor: '#efefef',
    backgroundPosition: 'center',
    backgroundSize: 'cover',
    borderRadius: 4,
    overflow: 'hidden'
  }
};

const selector = createSelector(
  movieFromPropsSelector,
  placeIdFromRouterSelector,
  (movie, placeId) => {
    return { movie, placeId };
  }
);

const actions = {
  loadMovie
};

const MovieListItemConnectedContainer = connect(selector, actions)(Radium(MovieListItem));

export default withRouter(MovieListItemConnectedContainer);