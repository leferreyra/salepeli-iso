// @flow
import React, { Component, Fragment } from 'react';
import Home from 'components/pages/Home';
import Start from 'components/pages/Start';
import Movie from 'components/pages/Movie';
import { connect } from 'react-redux';
import { createSelector } from 'reselect';
import { Helmet } from 'react-helmet';
import { withRouter, Route } from 'react-router-dom';
import { loadPlaceId, type LoadPlaceIdAction } from 'actions/AppActions';

import ICON from 'images/icon.png';

type Actions = {
  loadPlaceId: LoadPlaceIdAction;
}

type Props = Actions & {};

class App extends Component<Props> {

  componentDidMount() {
    this.props.loadPlaceId();
  }

  render() {
    return (
      <Fragment>
        <Helmet>
          <meta name="viewport" content="width=device-width" />
          <link rel="shortcut icon" href={ICON} />
        </Helmet>
        <Route exact path='/' component={Start} />
        <Route path='/p/:placeId' component={Home} />
        <Route path='/m/:movieId' component={Movie} />
      </Fragment>
    )
  }
}

const actions = {
  loadPlaceId
}

export default withRouter(connect(null, actions)(App));