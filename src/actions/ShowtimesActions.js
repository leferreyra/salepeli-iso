// @flow
import { createAction } from 'redux-actions';
import {
  LOAD_SHOWTIMES_START,
  LOAD_SHOWTIMES_SUCCESS,
  LOAD_SHOWTIMES_ERROR
} from 'constants/ActionConstants';
import { getShowtimes, type ShowtimesData } from 'apis/ShowtimesApi';
import { ShowtimesKeyRecord } from 'reducers/ShowtimesReducer';

import type { List } from 'immutable';
import type { ActionCreator } from 'actions/types';
import type { ThunkAction, Dispatch } from 'reducers/createStore';
import type { ShowtimeRecord } from 'records/ShowtimeRecord';

export type LoadShowtimesStartPayload = {
  key: ShowtimesKeyRecord;
};

export type LoadShowtimesErrorPayload = {
  key: ShowtimesKeyRecord;
};

export type LoadShowtimesSuccessPayload = {
  key: ShowtimesKeyRecord;
  showtimes: ShowtimesData;
};

const loadShowtimesStart: ActionCreator<LoadShowtimesStartPayload> = createAction(LOAD_SHOWTIMES_START);
const loadShowtimesSuccess: ActionCreator<LoadShowtimesSuccessPayload> = createAction(LOAD_SHOWTIMES_SUCCESS);
const loadShowtimesError: ActionCreator<LoadShowtimesErrorPayload> = createAction(LOAD_SHOWTIMES_ERROR);

type LoadShowtimesParams = {
  movieId: string;
  theaterId: string;
  fromDate: number;
  toDate: number;
};
export type LoadShowtimesAction = (params: LoadShowtimesParams) => ThunkAction;
export const loadShowtimes: LoadShowtimesAction = params => {

  const { movieId, theaterId, fromDate, toDate } = params;
  const key = new ShowtimesKeyRecord(params);

  return (dispatch: Dispatch) => {
    dispatch(loadShowtimesStart({ key }));

    return getShowtimes(params)
      .then((showtimes: ShowtimesData) => {
        dispatch(loadShowtimesSuccess({ key, showtimes }))
      });
  }
}

export type MoviesActions = {
  loadShowtimes: LoadShowtimesAction;
};