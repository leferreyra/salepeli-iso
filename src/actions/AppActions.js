// @flow
import Store from 'store';
import { createAction } from 'redux-actions';
import {
  LOAD_PLACE_ID_START,
  LOAD_PLACE_ID_SUCCESS,
  LOAD_PLACE_ID_ERROR
} from 'constants/ActionConstants';
import { getPlaceIdFromCoords } from 'apis/PlacesApi';

import type { ActionCreator, NoArgActionCreator } from 'actions/types';
import type { ThunkAction, Dispatch } from 'reducers/createStore';

export type LoadPlaceIdSuccessPayload = {
  placeId: string;
};

const loadPlaceIdStart : NoArgActionCreator = createAction(LOAD_PLACE_ID_START);
const loadPlaceIdSuccess: ActionCreator<LoadPlaceIdSuccessPayload> = createAction(LOAD_PLACE_ID_SUCCESS);
const loadPlaceIdError: ActionCreator<LoadPlaceIdSuccessPayload> = createAction(LOAD_PLACE_ID_ERROR);

export type GetLocation = () => ThunkAction;
export const getLocation: GetLocation = () => {
  return (dispatch: Dispatch) => {
    if ('geolocation' in navigator) {
      dispatch(loadPlaceIdStart());
      navigator.geolocation.getCurrentPosition(position => {
        const { latitude, longitude } = position.coords;
        return getPlaceIdFromCoords(latitude, longitude)
          .then(response => {
            const { placeId } = response;
            Store.set('placeId', placeId);
            dispatch(loadPlaceIdSuccess({ placeId }))
          });
      });
    }
  }
}

export type LoadPlaceIdAction = () => ThunkAction;
export const loadPlaceId: LoadPlaceIdAction = () => {
  return (dispatch: Dispatch) => {
    const placeId = Store.get('placeId');
    return dispatch(loadPlaceIdSuccess({ placeId }));
  }
}

export type AppActions = {
  loadPlaceId: LoadPlaceIdAction;
};