// @flow
import { createAction } from 'redux-actions';
import {
  LOAD_MOVIE_START,
  LOAD_MOVIE_SUCCESS,
  LOAD_MOVIE_ERROR
} from 'constants/ActionConstants';
import { MovieRecord } from 'records/MovieRecord';
import { getMovie } from 'apis/MoviesApi';

import type { MovieData } from 'apis/MoviesApi';
import type { ActionCreator } from 'actions/types';
import type { ThunkAction, Dispatch } from 'reducers/createStore';

export type LoadMovieStartPayload = {
  movieId: string;
};

export type LoadMovieErrorPayload = {
  movieId: string;
};

export type LoadMovieSuccessPayload = {
  movieId: string;
  movie: MovieRecord;
};

const loadMovieStart: ActionCreator<LoadMovieStartPayload> = createAction(LOAD_MOVIE_START);
const loadMovieSuccess: ActionCreator<LoadMovieSuccessPayload> = createAction(LOAD_MOVIE_SUCCESS);
const loadMovieError: ActionCreator<LoadMovieErrorPayload> = createAction(LOAD_MOVIE_ERROR);

export type LoadMovieAction = (movieId: string) => ThunkAction;
export const loadMovie: LoadMovieAction = movieId => {
  return (dispatch: Dispatch) => {
    dispatch(loadMovieStart({ movieId }));
    return getMovie(movieId)
      .then((response: MovieData) => {
        dispatch(loadMovieSuccess({
          movieId,
          movie: new MovieRecord(response)
        }))
      });
  }
}

export type MoviesActions = {
  loadMovie: LoadMovieAction;
};