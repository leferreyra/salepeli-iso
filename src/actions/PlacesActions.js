// @flow
import { createAction } from 'redux-actions';
import { get } from 'axios';
import {
  LOAD_PLACE_START,
  LOAD_PLACE_SUCCESS,
  LOAD_PLACE_ERROR
} from 'constants/ActionConstants';
import { PlaceRecord, createPlaceRecord } from 'records/PlaceRecord';
import { getPlace } from 'apis/PlacesApi';

import type { PlaceData } from 'apis/PlacesApi';
import type { ActionCreator } from 'actions/types';
import type { ThunkAction, Dispatch } from 'reducers/createStore';

export type LoadPlaceStartPayload = {
  placeId: string;
};

export type LoadPlaceErrorPayload = {
  placeId: string;
};

export type LoadPlaceSuccessPayload = {
  placeId: string;
  place: PlaceRecord;
};

const loadPlaceStart: ActionCreator<LoadPlaceStartPayload> = createAction(LOAD_PLACE_START);
const loadPlaceSuccess: ActionCreator<LoadPlaceSuccessPayload> = createAction(LOAD_PLACE_SUCCESS);
const loadPlaceError: ActionCreator<LoadPlaceErrorPayload> = createAction(LOAD_PLACE_ERROR);

export type LoadPlaceAction = (placeId: string) => ThunkAction;
export const loadPlace: LoadPlaceAction = placeId => {
  return (dispatch: Dispatch) => {
    dispatch(loadPlaceStart({ placeId }));
    return getPlace(placeId)
      .then((response: PlaceData) => {
        dispatch(loadPlaceSuccess({
          placeId,
          place: createPlaceRecord(response)
        }))
      });
  }
}

export type PlacesActions = {
  loadPlace: LoadPlaceAction;
};