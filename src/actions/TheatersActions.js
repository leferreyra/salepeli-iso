// @flow
import { createAction } from 'redux-actions';
import {
  LOAD_THEATER_START,
  LOAD_THEATER_SUCCESS,
  LOAD_THEATER_ERROR
} from 'constants/ActionConstants';
import { TheaterRecord } from 'records/TheaterRecord';
import { getTheater } from 'apis/TheatersApi';

import type { TheaterData } from 'apis/TheatersApi';
import type { ActionCreator } from 'actions/types';
import type { ThunkAction, Dispatch } from 'reducers/createStore';

export type LoadTheaterStartPayload = {
  theaterId: string;
};

export type LoadTheaterErrorPayload = {
  theaterId: string;
};

export type LoadTheaterSuccessPayload = {
  theaterId: string;
  theater: TheaterRecord;
};

const loadTheaterStart: ActionCreator<LoadTheaterStartPayload> = createAction(LOAD_THEATER_START);
const loadTheaterSuccess: ActionCreator<LoadTheaterSuccessPayload> = createAction(LOAD_THEATER_SUCCESS);
const loadTheaterError: ActionCreator<LoadTheaterErrorPayload> = createAction(LOAD_THEATER_ERROR);

export type LoadTheaterAction = (theaterId: string) => ThunkAction;
export const loadTheater: LoadTheaterAction = theaterId => {
  return (dispatch: Dispatch) => {
    dispatch(loadTheaterStart({ theaterId }));
    return getTheater(theaterId)
      .then((response: TheaterData) => {
        dispatch(loadTheaterSuccess({
          theaterId,
          theater: new TheaterRecord(response)
        }))
      });
  }
}

export type MoviesActions = {
  loadTheater: LoadTheaterAction;
};