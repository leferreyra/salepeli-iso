// @flow
export type Action<T> = { type: string, payload: T };
export type NoArgAction = () => Action<void>;
export type ActionCreator<T> = (payload: T) => Action<T>;
export type NoArgActionCreator = ActionCreator<void>;
