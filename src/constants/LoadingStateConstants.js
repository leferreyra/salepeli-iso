// @flow

export const INITIAL = 'INITIAL';
export const LOADING = 'LOADING';
export const LOADED = 'LOADED';
export const ERROR = 'ERROR';

export const LOADING_STATES = { INITIAL, LOADING, LOADED, ERROR };

export type LoadingState = $Keys<typeof LOADING_STATES>;