// @flow
import { handleActions } from 'redux-actions';
import { Record, Map, List } from 'immutable';
import {
  INITIAL,
  LOADING,
  LOADED,
  type LoadingState
} from 'constants/LoadingStateConstants';
import {
  LOAD_SHOWTIMES_START,
  LOAD_SHOWTIMES_SUCCESS,
  LOAD_SHOWTIMES_ERROR
} from 'constants/ActionConstants';
import { ShowtimeRecord, type ShowtimeData } from 'records/ShowtimeRecord';

import type { Action } from 'actions/types';
import type {
  LoadShowtimesStartPayload,
  LoadShowtimesSuccessPayload,
} from 'actions/ShowtimesActions';

export class ShowtimesStateRecord extends Record({
  state: INITIAL,
  showtimes: null
}) {
  state: LoadingState;
  showtimes: ?List<ShowtimeRecord>;
}

export class ShowtimesKeyRecord extends Record({
  movieId: '',
  theaterId: '',
  fromDate: 0,
  toDate: 0
}) {
  movieId: string;
  theaterId: string;
  fromDate: number;
  toDate: number;
}

type ShowtimeStateData = {
  state: LoadingState;
  showtimes: Array<ShowtimeData>;
};

type ShowtimeKeyData = {
  movieId: string;
  theaterId: string;
  fromDate: number;
  toDate: number;
};

export type ShowtimesStateData = Array<{
  key: ShowtimeKeyData;
  value: ShowtimeStateData;
}>;

const createShowtimesStateRecord = (showtimesState: ShowtimeStateData): ShowtimesStateRecord => {

  const showtimes = List(showtimesState.showtimes)
    .map(showtime => new ShowtimeRecord(showtime))

  return new ShowtimesStateRecord({
    state: showtimesState.state,
    showtimes
  });
}

export function createShowtimesStateMap(data: ShowtimesStateData): Map<ShowtimesKeyRecord, ShowtimesStateRecord> {
  return List(data)
    .reduce((showtimesMap, showtimes) => {

      const key = new ShowtimesKeyRecord(showtimes.key);
      const state = createShowtimesStateRecord(showtimes.value);

      return showtimesMap.set(key, state);
    }, Map())
};

const reducerMap = {};

reducerMap[LOAD_SHOWTIMES_START] = (state: Map<ShowtimesKeyRecord, ShowtimesStateRecord>, action: Action<LoadShowtimesStartPayload>) => {
  const { key } = action.payload;
  const showtimesState = state.get(key, new ShowtimesStateRecord());
  return state.set(key, showtimesState.set('state', LOADING));
};

reducerMap[LOAD_SHOWTIMES_SUCCESS] = (state: Map<ShowtimesKeyRecord, ShowtimesStateRecord>, action: Action<LoadShowtimesSuccessPayload>) => {
  const { key, showtimes } = action.payload;

  const records = Map(showtimes)
    .valueSeq()
    .toList()
    .map(showtime => new ShowtimeRecord(showtime));

  return state.withMutations(state => {
    state.setIn([key, 'state'], LOADED);
    state.setIn([key, 'showtimes'], records);
  });
};

export default function(initialState?: ShowtimesStateData): Object {
  return handleActions(reducerMap,
    initialState ? createShowtimesStateMap(initialState) : Map());
}
