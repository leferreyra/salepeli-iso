// @flow
import { handleActions } from 'redux-actions';
import { Record, Map } from 'immutable';
import {
  INITIAL,
  LOADED,
  LOADING,
  ERROR,
  type LoadingState
} from 'constants/LoadingStateConstants';
import {
  LOAD_MOVIE_START,
  LOAD_MOVIE_SUCCESS,
  LOAD_MOVIE_ERROR
} from 'constants/ActionConstants';
import type {
  LoadMovieStartPayload,
  LoadMovieSuccessPayload,
  LoadMovieErrorPayload
} from 'actions/MoviesActions';
import { MovieRecord } from 'records/MovieRecord';

import type { Action } from 'actions/types';
import type { MovieData } from 'apis/MoviesApi';

export class MovieStateRecord extends Record({
  state: INITIAL,
  movie: null
}) {
  state: LoadingState;
  movie: ?MovieRecord;
}

type MovieStateData = {
  state: LoadingState;
  movie: MovieData;
};

export type MoviesStateData = {
  [string]: MovieStateData;
};

export function createMovieStateRecordMap(data: MoviesStateData): Map<string, MovieStateRecord> {
  return Map(data)
    .map((movieState: MovieStateData) => new MovieStateRecord({
      state: movieState.state,
      movie: new MovieRecord(movieState.movie)
    }));
};

const reducerMap = {};

reducerMap[LOAD_MOVIE_START] = (state: Map<string, MovieStateRecord>, action: Action<LoadMovieStartPayload>) => {
  const { movieId } = action.payload;
  const placeState = state.get(movieId, new MovieStateRecord()).set('state', LOADING);
  return state.set(movieId, placeState);
};

reducerMap[LOAD_MOVIE_SUCCESS] = (state: Map<string, MovieStateRecord>, action: Action<LoadMovieSuccessPayload>) => {
  const { movieId, movie } = action.payload;
  return state.updateIn([movieId], placeState =>
    placeState.merge({ state: LOADED, movie }));
};

reducerMap[LOAD_MOVIE_ERROR] = (state: Map<string, MovieStateRecord>, action: Action<LoadMovieErrorPayload>) => {
  const { movieId } = action.payload;
  return state.setIn([movieId, 'state'], ERROR);
};

export default function(initialState?: MoviesStateData): Object {
  return handleActions(reducerMap,
    initialState ? createMovieStateRecordMap(initialState) : Map());
}
