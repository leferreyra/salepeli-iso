// @flow
import { handleActions } from 'redux-actions';
import { Record, Map } from 'immutable';
import { createPlaceRecord } from 'records/PlaceRecord';
import {
  INITIAL,
  LOADED,
  LOADING,
  ERROR,
  type LoadingState
} from 'constants/LoadingStateConstants';
import {
  LOAD_PLACE_START,
  LOAD_PLACE_SUCCESS,
  LOAD_PLACE_ERROR
} from 'constants/ActionConstants';

import type { Action } from 'actions/types';
import type { PlaceRecord } from 'records/PlaceRecord';
import type { PlaceData } from 'apis/PlacesApi';
import type {
  LoadPlaceStartPayload,
  LoadPlaceSuccessPayload,
  LoadPlaceErrorPayload
} from 'actions/PlacesActions';

export class PlaceStateRecord extends Record({
  state: INITIAL,
  place: null
}) {
  state: LoadingState;
  place: ?PlaceRecord;
}

type PlaceStateData = {
  state: LoadingState;
  place: PlaceData;
};

export type PlacesStateData = {
  [string]: PlaceStateData;
};

export function createPlaceStateRecordMap(data: PlacesStateData): Map<string, PlaceStateRecord> {
  return Map(data)
    .map((placeState: PlaceStateData) => new PlaceStateRecord({
      state: placeState.state,
      place: createPlaceRecord(placeState.place)
    }));
};

const reducerMap = {};

reducerMap[LOAD_PLACE_START] = (state: Map<string, PlaceStateRecord>, action: Action<LoadPlaceStartPayload>) => {
  const { placeId } = action.payload;
  const placeState = state.get(placeId, new PlaceStateRecord()).set('state', LOADING);
  return state.set(placeId, placeState);
};

reducerMap[LOAD_PLACE_SUCCESS] = (state: Map<string, PlaceStateRecord>, action: Action<LoadPlaceSuccessPayload>) => {
  const { placeId, place } = action.payload;
  return state.updateIn([placeId], placeState =>
    placeState.merge({ state: LOADED, place }));
};

reducerMap[LOAD_PLACE_ERROR] = (state: Map<string, PlaceStateRecord>, action: Action<LoadPlaceErrorPayload>) => {
  const { placeId } = action.payload;
  return state.setIn([placeId, 'state'], ERROR);
};

export default function(initialState?: PlacesStateData): Object {
  return handleActions(reducerMap,
    initialState ? createPlaceStateRecordMap(initialState) : Map());
}
