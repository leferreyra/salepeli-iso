// @flow
import { handleActions } from 'redux-actions';
import { Record, Map } from 'immutable';
import {
  INITIAL,
  LOADED,
  LOADING,
  ERROR,
  type LoadingState
} from 'constants/LoadingStateConstants';
import  { TheaterRecord } from 'records/TheaterRecord';
import {
  LOAD_THEATER_START,
  LOAD_THEATER_SUCCESS,
  LOAD_THEATER_ERROR
} from 'constants/ActionConstants';

import type { Action } from 'actions/types';
import type {
  LoadTheaterStartPayload,
  LoadTheaterSuccessPayload,
  LoadTheaterErrorPayload
} from 'actions/TheatersActions';
import  type { TheaterData } from 'apis/TheatersApi';

export class TheaterStateRecord extends Record({
  state: INITIAL,
  theater: null
}) {
  state: LoadingState;
  theater: ?TheaterRecord;
}

type TheaterStateData = {
  state: LoadingState;
  theater: TheaterData;
};

export type TheatersStateData = {
  [string]: TheaterStateData;
};

export function createTheaterStateRecordMap(data: TheatersStateData): Map<string, TheaterStateRecord> {
  return Map(data)
    .map((theaterState: TheaterStateData) => new TheaterStateRecord({
      state: theaterState.state,
      theater: new TheaterRecord(theaterState.theater)
    }));
};

const reducerMap = {};

reducerMap[LOAD_THEATER_START] = (state: Map<string, TheaterStateRecord>, action: Action<LoadTheaterStartPayload>) => {
  const { theaterId } = action.payload;
  const placeState = state.get(theaterId, new TheaterStateRecord()).set('state', LOADING);
  return state.set(theaterId, placeState);
};

reducerMap[LOAD_THEATER_SUCCESS] = (state: Map<string, TheaterStateRecord>, action: Action<LoadTheaterSuccessPayload>) => {
  const { theaterId, theater } = action.payload;
  return state.updateIn([theaterId], placeState =>
    placeState.merge({ state: LOADED, theater }));
};

reducerMap[LOAD_THEATER_ERROR] = (state: Map<string, TheaterStateRecord>, action: Action<LoadTheaterErrorPayload>) => {
  const { theaterId } = action.payload;
  return state.setIn([theaterId, 'state'], ERROR);
};

export default function(initialState?: TheatersStateData): Object {
  return handleActions(reducerMap,
    initialState ? createTheaterStateRecordMap(initialState) : Map());
}
