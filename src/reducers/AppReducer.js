// @flow
import { handleActions } from 'redux-actions';
import { Record, Map, List } from 'immutable';
import { createPlaceRecord } from 'records/PlaceRecord';
import {
  LOAD_PLACE_ID_START,
  LOAD_PLACE_ID_SUCCESS
} from 'constants/ActionConstants';
import {
  INITIAL,
  LOADED,
  LOADING,
  ERROR,
  type LoadingState
} from 'constants/LoadingStateConstants';

import type { Action, NoArgAction } from 'actions/types';
import type {
  LoadPlaceIdSuccessPayload
} from 'actions/AppActions';

export class GeolocationStateRecord extends Record({
  state: INITIAL,
  placeId: null
}) {
  state: LoadingState;
  placeId: ?string;
}

export class AppStateRecord extends Record({
  geolocation: new GeolocationStateRecord(),
  latestMovies: List()
}) {
  geolocation: ?GeolocationStateRecord;
  latestMovies: List<string>;
}

export type AppStateData = {
  latestMovies: Array<string>;
};

const createAppStateRecord = (data: AppStateData): AppStateRecord => {
  return new AppStateRecord({
    latestMovies: List(data.latestMovies)
  });
};

const reducerMap = {};

reducerMap[LOAD_PLACE_ID_START] = (state: AppStateRecord, action: NoArgAction): AppStateRecord => {
  return state.setIn(['geolocation', 'state'], LOADING);
}

reducerMap[LOAD_PLACE_ID_SUCCESS] = (state: AppStateRecord, action: Action<LoadPlaceIdSuccessPayload>): AppStateRecord => {
  const { placeId } = action.payload;

  return state.set('geolocation', new GeolocationStateRecord({
    state: LOADED,
    placeId
  }));
}

export default function(initialState?: AppStateData): Object {
  return handleActions(reducerMap, initialState ? createAppStateRecord(initialState) : new AppStateRecord());
}
