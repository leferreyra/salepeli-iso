// @flow

import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import { applyMiddleware, createStore, compose } from 'redux';
import { combineReducers } from 'redux';

import AppReducer, {
  type AppStateData,
  type AppStateRecord
} from 'reducers/AppReducer';
import PlacesReducer, { type PlacesStateData } from 'reducers/PlacesReducer';
import TheatersReducer, { type TheatersStateData } from 'reducers/TheatersReducer';
import MoviesReducer, { type MoviesStateData } from 'reducers/MoviesReducer';
import ShowtimesReducer, {
  type ShowtimesStateData,
  type ShowtimesKeyRecord,
  type ShowtimesStateRecord
} from 'reducers/ShowtimesReducer';

import type { PlaceRecord } from 'records/PlaceRecord';
import type { TheaterRecord } from 'records/TheaterRecord';
import type { MovieRecord } from 'records/MovieRecord';
import type { Action } from 'actions/types';

type InitialStateParam = {
  app?: AppStateData;
  places?: PlacesStateData;
  theaters?: TheatersStateData;
  movies?: MoviesStateData;
  showtimes?: ShowtimesStateData;
};

export type State = {
  app: AppStateRecord;
  places: Map<string, PlaceRecord>;
  theaters: Map<string, TheaterRecord>;
  movies: Map<string, MovieRecord>;
  showtimes: Map<ShowtimesKeyRecord, ShowtimesStateRecord>;
};

export type GetState = () => State;
export type ThunkAction = (dispatch: Dispatch, getState: GetState) => any;
export type Dispatch = (action: Action<any> | ThunkAction) => any;

export default function(initialState: InitialStateParam) {

  const rootReducer = combineReducers({
    app: AppReducer(initialState.app),
    places: PlacesReducer(initialState.places),
    theaters: TheatersReducer(initialState.theaters),
    movies: MoviesReducer(initialState.movies),
    showtimes: ShowtimesReducer(initialState.showtimes)
  });

  const middlewares = [thunkMiddleware];

  if (process.env.NODE_ENV === 'development') {
    const logger = createLogger({
      collapsed: true
    });
    middlewares.push(logger);
  }

  return compose(applyMiddleware(...middlewares))(createStore)(rootReducer);
}