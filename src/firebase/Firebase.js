
import Firebase from 'firebase/app';
import 'firebase/firestore';

Firebase.initializeApp({
  "apiKey": "AIzaSyDOccZ65J6kic3ZTrmsIPniOqjbHkDPipc",
  "databaseURL": "https://sale-peli.firebaseio.com",
  "storageBucket": "sale-peli.appspot.com",
  "authDomain": "sale-peli.firebaseapp.com",
  "messagingSenderId": "926956951248",
  "projectId": "sale-peli"
});

const firestore = Firebase.firestore();

firestore.settings({
  timestampsInSnapshots: true
});

export const database = firestore;