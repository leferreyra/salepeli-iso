
const admin = require('firebase-admin');
const functions = require('firebase-functions');
admin.initializeApp(functions.config().firebase);

const app = require('./src/app');
const updateScrapedData = require('./src/updateScrapedData');

// App
exports.app = app;

// PubSub
exports.updateScrapedData = updateScrapedData;