const admin = require('firebase-admin');
const firestore = admin.firestore();
const functions = require('firebase-functions');

const TYPE_COLLECTIONS = {
  'movie': 'movies',
  'theater': 'theaters',
  'showtime': 'showtimes'
};

const getPathForData = (type, data) => {
  const collection = TYPE_COLLECTIONS[type];
  return `${collection}/${data.id}`;
}

module.exports = functions.pubsub.topic('update-scraped-data')
  .onPublish(message => {

    const { type } = message.attributes;
    const path = getPathForData(type, message.json);

    return firestore
      .doc(path)
      .set(message.json);
  });
