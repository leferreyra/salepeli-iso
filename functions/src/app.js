const admin = require('firebase-admin');
const functions = require('firebase-functions');
const express = require('express');
const compression = require('compression');
const moment = require('moment-timezone');
const values = require('lodash.values');
const find = require('lodash.find');
const React = require('react');
const ReactDOMServer = require('react-dom/server');
const Helmet = require('react-helmet').default;
const Provider = require('react-redux').Provider;
const template = require('./templates/template');

const { ServerApp, Database, createStore } = require('../build/server.bundle.js').default;
const App = React.createFactory(ServerApp);

const database = new Database(admin.firestore());

const {
  getPlace,
  getPlaceFromTheater,
  getPlaceIdFromCoords,
  getNearbyTheaters
} = require('./utils/geolocation');

function render(req, res, state) {

  res.set('Cache-Control', 'public, max-age=1800, s-maxage=3600');

  const { url } = req;
  const radiumConfig = { userAgent: req.headers['user-agent'] };

  const store = createStore(state);

  const body = ReactDOMServer.renderToString(
    React.createElement(Provider, { store }, App({ url, radiumConfig, context: {} }))
  );

  const helmet = Helmet.renderStatic();

  return template({ body, helmet, initialState: state });
}

function buildLoadedObjectMap(objects, name) {
  return objects 
    .reduce((objectMap, object) => {
      objectMap[object.id] = {
        state: 'LOADED',
        [name]: object
      };
      return objectMap;
    }, {});
}

const app = express();
app.use(compression());

// Index 
app.get('/', (req, res) => {
  return database
    .getMovies()
    .then(movies => {
      const latest = values(movies).slice(0, 10);
      const latestMovies = latest.map(m => m.id);
      return res.send(render(req, res, {
        app: { latestMovies },
        movies: buildLoadedObjectMap(latest, 'movie')
      }));
    })
});

// Place ID 
app.get('/api/coords/:coords', (req, res) => {
  // coords = '239.9435,29.349585'
  const { coords } = req.params;

  return getPlaceIdFromCoords(coords)
    .then(placeId => res.send(JSON.stringify({ placeId })))
    .catch(err => console.error(err));
});

// Place JSON
app.get('/api/place/:placeId', (req, res) => {
  // placeId = 'resistencia-chaco'
  const { placeId } = req.params;

  // TODO: move this to a common place with render
  res.set('Cache-Control', 'public, max-age=1800, s-maxage=3600');

  Promise.all([
    getPlace(placeId),
    database.getTheaters()
  ])
    .then(([place, theaters]) => {
      const nearbyTheaters = getNearbyTheaters(place, theaters);
      const nearbyTheatersIds = nearbyTheaters.map(t => t.theaterId);

      return Promise.all([
        place,
        nearbyTheaters,
        database.getTheatersMovies(nearbyTheatersIds)
      ]);
    })
    .then(([place, nearbyTheaters, nearbyMovies]) => {
      return Object.assign(place, {
        nearbyTheaters,
        nearbyMovies
      });
    })
    .then(response => res.send(JSON.stringify(response)))
    .catch(err => console.error(err));
});

// Place
app.get('/p/:placeId', (req, res) => {
  const { placeId } = req.params;

  database.getTheater(placeId)
    .then(theater => {
      return Promise.all([
        theater ?
          getPlaceFromTheater(theater):
          getPlace(placeId),
        database.getTheaters(),
        database.getMovies()
      ]);
    })
    .then(([place, theaters, movies]) => {

      const nearbyTheaters = getNearbyTheaters(place, theaters);
      const nearbyTheatersIds = nearbyTheaters.map(t => t.theaterId);

      return Promise.all([
        place,
        theaters,
        movies,
        nearbyTheaters,
        database.getTheatersMovies(nearbyTheatersIds)
      ]);
    })
    .then(([place, theaters, movies, nearbyTheaters, nearbyMovies]) => {

      return res.send(render(req, res, {

        places: buildLoadedObjectMap([
          Object.assign(place, { nearbyTheaters, nearbyMovies })
        ], 'place'),

        theaters: buildLoadedObjectMap(
          nearbyTheaters.map(t => theaters[t.theaterId])
        , 'theater'),

        movies: buildLoadedObjectMap(
          nearbyMovies.map(movieId => movies[movieId])
        , 'movie'),

      }));

    })
    .catch(err => console.error(err));
});

// Movie showtimes
app.get('/m/:movieId/s/:placeId', (req, res) => {
  const { placeId, movieId } = req.params;

  return database.getTheater(placeId)
    .then(theater => {
      return Promise.all([
        theater ?
          getPlaceFromTheater(theater):
          getPlace(placeId),
        database.getTheaters(),
        database.getMovie(movieId)
      ]);
    })
    .then(([place, theaters, movie]) => {

      const date = place.timezone ? moment().tz(place.timezone) : moment();

      const fromDate = date.startOf('day');
      const toDate = date.endOf('day');

      const nearbyTheaters = getNearbyTheaters(place, theaters);
      const nearbyTheatersIds = nearbyTheaters.map(t => t.theaterId);

      const showtimesPromise = Promise.all(
        nearbyTheatersIds
          .map(theaterId => database.getMovieShowtimes(
            movieId,
            theaterId,
            fromDate.toDate(),
            toDate.toDate()
          ))
      );

      return Promise.all([
        place,
        theaters,
        movie,
        nearbyTheaters,
        database.getTheatersMovies(nearbyTheatersIds),
        showtimesPromise,
        fromDate,
        toDate
      ]);
    })
    .then(([place, theaters, movie, nearbyTheaters, nearbyMovies, showtimes, fromDate, toDate]) => {

      if (!movie || !place) {
        res.redirect('/');
      }

      return res.send(render(req, res, {

        places: buildLoadedObjectMap([
          Object.assign(place, { nearbyTheaters, nearbyMovies })
        ], 'place'),

        theaters: buildLoadedObjectMap(
          nearbyTheaters.map(t => theaters[t.theaterId])
        , 'theater'),

        movies: buildLoadedObjectMap([movie], 'movie'),

        showtimes: nearbyTheaters
          .map(t => t.theaterId)
          .map((theaterId, idx) => {
            return {
              key: {
                theaterId,
                movieId,
                fromDate: fromDate.unix(),
                toDate: toDate.unix()
              },
              value: {
                state: 'LOADED',
                showtimes: values(showtimes[idx])
              }
            }
          })

      }));

    })
    .catch(err => console.error(err));
});

function renderMovie(req, res) {
  const { movieId } = req.params;

  return database.getMovie(movieId)
    .then(movie => {

      if (!movie) {
        res.redirect('/');
      }

      return res.send(render(req, res, {
        movies: buildLoadedObjectMap([movie], 'movie')
      }));
    })
    .catch(err => console.error(err));
}

app.get('/m/:movieId', renderMovie);

// Movie info
app.get('/m/:movieId/s', renderMovie);

// Sitemaps
app.get('/sitemaps/theaters.txt', (req, res) => {
  return database.getTheaters()
    .then(theaters => {
      let urls = '';

      values(theaters)
        .forEach(theater => urls += `https://salepeli.com/p/${theater.id}\n`);

      return res.send(urls);
    });
});

app.get('/sitemaps/movies.txt', (req, res) => {
  return database.getMovies()
    .then(movies => {
      let urls = '';

      values(movies)
        .forEach(movie => urls += `https://salepeli.com/m/${movie.id}\n`);

      return res.send(urls);
    });
});

module.exports = functions.https.onRequest(app);
