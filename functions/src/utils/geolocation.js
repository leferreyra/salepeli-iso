
const gmaps = require('@google/maps');
const turf = require('@turf/turf');
const moment = require('moment-timezone');
const slugify = require('slugify');
const get = require('lodash.get');
const values = require('lodash.values');
const find = require('lodash.find');

const API_KEY = 'AIzaSyA2bxbrKNySdYYb4LocysM9W3NNRZP-XFo';

const client = gmaps.createClient({
  key: API_KEY,
  Promise
});

const findAddressComponentByType = type => result => {
  const types = get(result, 'types', []);
  return find(types, t => t === type);
};

const findCountryTimezone = country => {
  return find(moment.tz.names(), tz => tz.indexOf(country) !== -1);
}

const getPlaceIdFromResponse = response => {
  const components = get(response, 'json.results', []);

  const locality = find(components, findAddressComponentByType('locality'));
  const sublocality = find(components, findAddressComponentByType('sublocality'));

  let address = get(locality || sublocality, 'formatted_address', '');
  address = address.replace(/Province/g, '');

  return slugify(address, { lower: true });
}

const getPlaceIdFromCoords = coords => {

  const latlng = coords
    .split(',')
    .map(f => parseFloat(f));

  return client
    .reverseGeocode({ latlng })
    .asPromise()
    .then(getPlaceIdFromResponse);
}

const getPlace = placeId => {

  const address = placeId 
    .split('-')
    .join(' ');

  return client
    .geocode({ address })
    .asPromise()
    .then(response => {
      const location = get(response, 'json.results[0].geometry.location', null);
      const name = get(response, 'json.results[0].address_components[0].long_name');

      const components = get(response, 'json.results[0].address_components', []);
      const countryComponent = find(components, findAddressComponentByType('country'));
      const country = get(countryComponent, 'long_name', '');

      const timezone = findCountryTimezone(country);

      return location ? {
        id: placeId,
        name,
        location,
        timezone 
      } : null;
    });
};


const getPlaceFromTheater = theater => {
  const { id, name, latitude, longitude } = theater;

  return {
    id,
    name,
    location: {
      lat: latitude,
      lng: longitude
    },
    timezone: 'America/Argentina/Buenos_Aires'
  };
}

const getNearbyTheaters = (place, theaters) => {
  const { lat, lng } = place.location;
  const point = turf.point([lat, lng]);

  return values(theaters)
    .map(theater => {
      const { latitude, longitude } = theater;
      const theaterPoint = turf.point([latitude, longitude]);
      const distance = turf.distance(point, theaterPoint);

      return {
        theaterId: theater.id,
        distance
      };
    })
    .filter(theater => theater.distance < 20)
    .sort((a, b) => a.distance - b.distance)
    .slice(0, 5);
};

module.exports = {
  getPlace,
  getPlaceFromTheater,
  getPlaceIdFromCoords,
  getNearbyTheaters
};