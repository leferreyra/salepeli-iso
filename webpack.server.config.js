
const merge = require('webpack-merge');
const path = require('path');
const config = require('./webpack.config');

module.exports = merge(config, {
  entry: {
    server: './src/containers/server.js'
  },
  mode: 'production',
  output: {
    path: path.resolve(__dirname, './functions/build'),
    filename: '[name].bundle.js',
    libraryTarget: 'commonjs2'
  },
  module: {
    rules: [{
      test: path.resolve(__dirname, 'src/database/ClientDatabase'),
      use: 'null-loader'
    }]
  },
  externals: ['react-helmet']
});